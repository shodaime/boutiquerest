package entities;


import java.time.LocalDate;
import java.util.Objects;


public class Client extends Personne{
    private String carteVisa;
    private String cin;

    public Client(){super();}
    public Client(int id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
    }
    public Client(String carteVisa, int id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.carteVisa = carteVisa;
    }
    public Client(int id, String nom, String prenom, LocalDate dateNaissance,String cin) {
        super(id, nom, prenom, dateNaissance);
        this.cin=cin;
    }
    public Client(String carteVisa, int id, String nom, String prenom, LocalDate dateNaissance,String cin) {
        super(id, nom, prenom, dateNaissance);
        this.carteVisa = carteVisa;
        this.cin=cin;
    }

    public Client(int id, String nom, String prenom, LocalDate date, String cin, String cartevisa) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String getCin() {return cin;}
    public void setCin(String cin) {this.cin = cin;}
    public String getCarteVisa() {return carteVisa;}
    public void setCarteVisa(String carteVisa) {this.carteVisa = carteVisa;}

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.carteVisa);
        hash = 73 * hash + Objects.hashCode(this.cin);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
         if (obj == null || this.getClass()!=obj.getClass() ) {
            return false;
        }
        final Client other = (Client) obj;
        return this.id == other.id;
    }


    @Override
    public String toString() {
        return "Client\n identifiant= " + this.getId() + ",\n nom= " + this.getNom() + ",\n prenoms= " + this.getPrenom() + ",\n dateNaissance= " + this.getDateNaissance()+ ",\n carteVisa=" + carteVisa + ",\n cin=" + cin ;
    }
    
}
