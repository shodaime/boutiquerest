package entities;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class Personne {
    protected int id;
    protected String nom;
    protected String prenom;
    protected LocalDate dateNaissance;
    

    public Personne(){}
    public Personne(int id, String nom, String prenom, LocalDate dateNaissance) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    public String getNom() {return nom;}
    public void setNom(String nom) {this.nom = nom;}
    public String getPrenom() {return prenom;}
    public void setPrenom(String prenom) {this.prenom = prenom;}
    public LocalDate getDateNaissance() {return dateNaissance;}
    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    public int getAge(){
        Date date = new Date();  
        return date.getYear() - this.dateNaissance.getYear();
    }
    public int getAge(LocalDate ref){
        return ref.getYear()-this.dateNaissance.getYear();
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 23 * hash + Objects.hashCode(this.nom);
        hash = 23 * hash + Objects.hashCode(this.prenom);
        hash = 23 * hash + Objects.hashCode(this.dateNaissance);
        return hash;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass()!=obj.getClass() ) {
            return false;
        }
        final Personne other = (Personne) obj;
        return this.id == other.id;
    }
    @Override
    public String toString() {
        return "Personne\n identifiant= " + id + ",\n nom= " + nom + ",\n prenoms= " + prenom + ",\n dateNaissance= " + dateNaissance ;
    }
}
