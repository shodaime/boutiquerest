package rs;

import entities.Employe;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.EmployeService;

@Path("/employes")
public class EmployeServiceResource {
    @POST
    @Path("/ajouter")
    public String Ajouter(@FormParam("id") int id,
            @FormParam("nom") String nom,
            @FormParam("prenom") String prenom,
            @FormParam("dateDeNaissance") LocalDate date,
            @FormParam("cnss") String cnss){
        Employe Employe= new Employe(id,nom,prenom,date,cnss);
        EmployeService.ajouter(Employe);
        return "Employe ajouté";
    }
    
    
    
    @POST
    @Path("/modifier")
    public String Modifier(@FormParam("id") int id,
            @FormParam("nom") String nom,
            @FormParam("prenom") String prenom,
            @FormParam("dateDeNaissance") LocalDate date,
            @FormParam("cnss") String cnss){
        Employe Employe= new Employe(id,nom,prenom,date,cnss);
        EmployeService.modifier(Employe);
        return "Employe modifié";
    }
    
    
    @GET
    @Path("/trouver")
    public Employe trouver(@QueryParam("id") int id){
        return EmployeService.trouver(id);
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("id") int id){
        EmployeService.supprimer(id);
        return "L' employe a été supprimée";
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("nom") int id,
            @QueryParam("nom") String nom,
            @QueryParam("prenom") String prenom,
            @QueryParam("dateDeNaissance") LocalDate date,
            @QueryParam("cnss") String cnss){
        Employe Employe;
        Employe = new Employe(id,nom,prenom,date,cnss);
        EmployeService.supprimer(Employe);
        return "L'employé a été supprimée";
    }
    
    @GET
    @Path("/lister")
    public List<Employe>  lister(){return EmployeService.lister();}
    
    @GET
    @Path("/lister")
    public List<Employe>  lister(
            @QueryParam("debut") int debut,
            @QueryParam("number") int nombre)
       {return EmployeService.lister(debut,nombre);}
}
