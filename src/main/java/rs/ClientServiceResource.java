package rs;

import entities.Client;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.ClientService;

@Path("/clients")
public class ClientServiceResource {
    @POST
    @Path("/ajouter")
    public String Ajouter(@FormParam("id") int id,
            @FormParam("nom") String nom,
            @FormParam("prenom") String prenom,
            @FormParam("dateDeNaissance") LocalDate date,
            @FormParam("cin") String cin,
            @FormParam("cartevisa") String cartevisa){
        Client Client= new Client(id,nom,prenom,date,cin,cartevisa);
        ClientService.ajouter(Client);
        return "Client ajouté";
    }
    
    
    
    @POST
    @Path("/modifier")
    public String Modifier(@FormParam("id") int id,
            @FormParam("nom") String nom,
            @FormParam("prenom") String prenom,
            @FormParam("dateDeNaissance") LocalDate date,
            @FormParam("cin") String cin,
            @FormParam("cartevisa") String cartevisa){
        Client Client= new Client(id,nom,prenom,date,cin,cartevisa);
        ClientService.modifier(Client);
        return "Client modifié";
    }
    
    
    @GET
    @Path("/trouver")
    public Client trouver(@QueryParam("id") int id){
        return ClientService.trouver(id);
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("id") int id){
        ClientService.supprimer(id);
        return "Le Client a été supprimée";
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("nom") int id,
            @QueryParam("nom") String nom,
            @QueryParam("prenom") String prenom,
            @QueryParam("dateDeNaissance") LocalDate date,
            @QueryParam("cin") String cin,
            @QueryParam("cartevisa") String cartevisa){
        Client Client;
        Client = new Client(id,nom,prenom,date,cin,cartevisa);
        ClientService.supprimer(Client);
        return "La catgorie a été supprimée";
    }
    
    @GET
    @Path("/lister")
    public List<Client>  lister(){return ClientService.lister();}
    
    @GET
    @Path("/lister")
    public List<Client>  lister(
            @QueryParam("debut") int debut,
            @QueryParam("number") int nombre)
       {return ClientService.lister(debut,nombre);}
}
