
package rs;

import entities.Categorie;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.CategorieService;

@Path("/categorie")
public class CategorieServiceResource {
    
    @POST
    @Path("/ajouter")
    public String Ajouter(@FormParam("id") int id,
            @FormParam("libelle") String libelle,
            @FormParam("description") String description){
        Categorie categorie= new Categorie(id,libelle,description);
        CategorieService.ajouter(categorie);
        return "produit ajouté";
    }
    
    @POST
    @Path("/modifier")
    public String Modifier(@FormParam("id") int id,
            @FormParam("libelle") String libelle,
            @FormParam("description") String description){
        Categorie categorie= new Categorie(id,libelle,description);
        CategorieService.modifier(categorie);
        return "produit modifié";
    }
    
    
    @GET
    @Path("/trouver")
    public Categorie trouver(@QueryParam("id") int id){
        return CategorieService.trouver(id);
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("id") int id){
        CategorieService.supprimer(id);
        return "La catgorie a été supprimée";
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("id") int id,
            @QueryParam("libelle") String libelle,
            @QueryParam("description") String description){
        Categorie categorie;
        categorie = new Categorie(id,libelle,description);
        CategorieService.supprimer(categorie);
        return "La catgorie a été supprimée";
    }
    
    @GET
    @Path("/lister")
    public List<Categorie>  lister(){return CategorieService.lister();}
    
    @GET
    @Path("/lister")
    public List<Categorie>  lister(
            @QueryParam("debut") int debut,
            @QueryParam("number") int nombre)
       {return CategorieService.lister(debut,nombre);}
    
   
}
