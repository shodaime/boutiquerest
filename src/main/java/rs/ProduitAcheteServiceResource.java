
package rs;

import entities.Produit;
import entities.ProduitAchete;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.ProduitAcheteService;

@Path("/produitAchete")
public class ProduitAcheteServiceResource {
    @POST
    @Path("/ajouter")
    public String Ajouter(@FormParam("produit") Produit produit,
            @FormParam("quantite") int quantite,
            @FormParam("remise") double remise){
        ProduitAchete ProduitAchete= new ProduitAchete(produit,quantite,remise);
        ProduitAcheteService.ajouter(ProduitAchete);
        return "ProduitAchete ajouté";
    }
    
    
    
   
   
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("produit") Produit produit,
            @QueryParam("quantite") int quantite,
            @QueryParam("remise") double remise){
        ProduitAchete ProduitAchete;
        ProduitAchete = new ProduitAchete(produit,quantite,remise);
        ProduitAcheteService.supprimer(ProduitAchete);
        return "L'ProduitAchete a été supprimée";
    }
    
    @GET
    @Path("/lister")
    public List<ProduitAchete>  lister(){return ProduitAcheteService.lister();}
    
    @GET
    @Path("/lister")
    public List<ProduitAchete>  lister(
            @QueryParam("debut") int debut,
            @QueryParam("number") int nombre)
       {return ProduitAcheteService.lister(debut,nombre);}
}
