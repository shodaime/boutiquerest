/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;


import entities.Categorie;
import entities.Produit;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.ProduitService;

@Path("/produits")
public class ProduitServiceResource {
    @POST
    @Path("/ajouter")
    public String Ajouter(@FormParam("id") int id,
            @FormParam("libelle") String libelle,
            @FormParam("prixUnitaire") double prixUnitaire,
            @FormParam("datePeremption") LocalDate date,
            @FormParam("categorie") Categorie categorie){
        Produit Produit= new Produit(id,libelle,prixUnitaire,date,categorie);
        ProduitService.ajouter(Produit);
        return "Produit ajouté";
    }
    
    
    
    @POST
    @Path("/modifier")
    public String Modifier(@FormParam("id") int id,
            @FormParam("libelle") String libelle,
            @FormParam("prixUnitaire") double prixUnitaire,
            @FormParam("datePeremption") LocalDate date,
            @FormParam("categorie") Categorie categorie){
        Produit Produit= new Produit(id,libelle,prixUnitaire,date,categorie);
        ProduitService.modifier(Produit);
        return "Produit modifié";
    }
    
    
    @GET
    @Path("/trouver")
    public Produit trouver(@QueryParam("id") int id){
        return ProduitService.trouver(id);
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("id") int id){
        ProduitService.supprimer(id);
        return "Le Produit a été supprimée";
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@FormParam("id") int id,
            @FormParam("libelle") String libelle,
            @FormParam("prixUnitaire") double prixUnitaire,
            @FormParam("datePeremption") LocalDate date,
            @FormParam("categorie") Categorie categorie){
        Produit Produit;
        Produit = new Produit(id,libelle,prixUnitaire,date,categorie);
        ProduitService.supprimer(Produit);
        return "La Produit a été supprimée";
    }
    
    @GET
    @Path("/lister")
    public List<Produit>  lister(){return ProduitService.lister();}
    
    @GET
    @Path("/lister")
    public List<Produit>  lister(
            @QueryParam("debut") int debut,
            @QueryParam("number") int nombre)
       {return ProduitService.lister(debut,nombre);}
}
