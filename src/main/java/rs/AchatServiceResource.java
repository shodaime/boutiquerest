
package rs;

import entities.Achat;
import entities.ProduitAchete;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.AchatService;

@Path("/achats")
public class AchatServiceResource {
    @POST
    @Path("/ajouter")
    public String Ajouter(@FormParam("id") int id,
            @FormParam("remise") double remise,
            @FormParam("dateAchat") LocalDate date,
            @FormParam("ProduitAchete") ProduitAchete produitAchete){
        Achat Achat= new Achat(id,remise,date,produitAchete);
        AchatService.ajouter(Achat);
        return "Achat ajouté";
    }
    
    
    
    @POST
    @Path("/modifier")
    public String Modifier(@FormParam("id") int id,
            @FormParam("remise") double remise,
            @FormParam("dateAchat") LocalDate date,
            @FormParam("ProduitAchete") ProduitAchete produitAchete){
        Achat achat= new Achat(id,remise,date,produitAchete);
        AchatService.modifier(achat);
        return "Achat modifié";
    }
    
    
    @GET
    @Path("/trouver")
    public Achat trouver(@QueryParam("id") int id){
        return AchatService.trouver(id);
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("id") int id){
        AchatService.supprimer(id);
        return "Le Achat a été supprimée";
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("id") int id,
            @QueryParam("remise") double remise,
            @QueryParam("dateAchat") LocalDate date,
            @QueryParam("ProduitAchete") ProduitAchete produitAchete){
        Achat achat;
        achat = new Achat(id,remise,date,produitAchete);
        AchatService.supprimer(achat);
        return "L'Achat a été supprimée";
    }
    
    @GET
    @Path("/lister")
    public List<Achat>  lister(){return AchatService.lister();}
    
    @GET
    @Path("/lister")
    public List<Achat>  lister(
            @QueryParam("debut") int debut,
            @QueryParam("number") int nombre)
       {return AchatService.lister(debut,nombre);}
}
