package rs;

import entities.Personne;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.PersonneService;


@Path("/personne")
public class PersonneServiceResource {
    @POST
    @Path("/ajouter")
    public String Ajouter(@FormParam("id") int id,
            @FormParam("nom") String nom,
            @FormParam("prenom") String prenom,
            @FormParam("dateDeNaissance") LocalDate date){
        Personne Personne= new Personne(id,nom,prenom,date);
        PersonneService.ajouter(Personne);
        return "Personne ajouté";
    }
    
    
    
    @POST
    @Path("/modifier")
    public String Modifier(@FormParam("id") int id,
            @FormParam("nom") String nom,
            @FormParam("prenom") String prenom,
            @FormParam("dateDeNaissance") LocalDate date){
        Personne Personne= new Personne(id,nom,prenom,date);
        PersonneService.modifier(Personne);
        return "Personne modifié";
    }
    
    
    @GET
    @Path("/trouver")
    public Personne trouver(@QueryParam("id") int id){
        return PersonneService.trouver(id);
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("id") int id){
        PersonneService.supprimer(id);
        return "Le Personne a été supprimée";
    }
    
    @GET
    @Path("/supprimer")
    public String supprimer(@QueryParam("nom") int id,
            @QueryParam("nom") String nom,
            @QueryParam("prenom") String prenom,
            @QueryParam("dateDeNaissance") LocalDate date){
        Personne Personne;
        Personne = new Personne(id,nom,prenom,date);
        PersonneService.supprimer(Personne);
        return "La catgorie a été supprimée";
    }
    
    @GET
    @Path("/lister")
    public List<Personne>  lister(){return PersonneService.lister();}
    
    @GET
    @Path("/lister")
    public List<Personne>  lister(
            @QueryParam("debut") int debut,
            @QueryParam("number") int nombre)
       {return PersonneService.lister(debut,nombre);}
}
