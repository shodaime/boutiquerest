package service;

import entities.Client;
import java.util.List;

public class ClientService {
    static List<Client> liste;
    
    public static void ajouter(Client e){liste.add(e);}; 
    // ajoute l'objet e dans la collection liste
    
    
    public static void modifier(Client e){
        for(int i=0;i<liste.size();i++){
            if(liste.get(i).getId()==e.getId()){
                liste.remove(i);
                liste.add(i, e);
            }
        }
       
    }; 
    // remplace par e, l'objet Categorie de la liste qui a même id que e
    
    
    public static Client trouver(int id){return liste.get(id);};
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    
    
    public static void supprimer(int id){liste.remove(id);}; 
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    
    
    public static void supprimer(Client e){liste.remove(e);};
    // retirer de la liste, l'objet Categorie passé en paramètre
    
    
    public static List<Client> lister(){return liste;}; 
    // renvoyer tous les éléments de la liste
    
    
    public static List<Client> lister(int debut, int nombre){
        return liste.subList(debut, debut+nombre);
    }; 
    // renvoyer nombre éléments de la liste, commençant à la position debut
}
