package service;

import entities.Achat;
import java.util.List;

public class AchatService {
    static List<Achat> liste;
    
    public static void ajouter(Achat e){liste.add(e);}; 
    // ajoute l'objet e dans la collection liste
    
    
    public static void modifier(Achat e){
        for(int i=0;i<liste.size();i++){
            if(liste.get(i).getId()==e.getId()){
                liste.remove(i);
                liste.add(i, e);
            }
        }
       
    }; 
    // remplace par e, l'objet Achat de la liste qui a même id que e
    
    
    public static Achat trouver(int id){return liste.get(id);};
    // renvoie l'objet Achat de la liste qui a l'id passé en paramètre
    
    
    public static void supprimer(int id){liste.remove(id);}; 
    // retirer de la liste, l'objet Achat qui a l'id passé en paramètre
    
    
    public static void supprimer(Achat e){liste.remove(e);};
    // retirer de la liste, l'objet Achat passé en paramètre
    
    
    public static List<Achat> lister(){return liste;}; 
    // renvoyer tous les éléments de la liste
    
    
    public static List<Achat> lister(int debut, int nombre){
        return liste.subList(debut, debut+nombre);
    }; 
    // renvoyer nombre éléments de la liste, commençant à la position debut
}
