
package service;

import entities.Employe;
import java.util.List;


public class EmployeService {
    static List<Employe> liste;
    
    public static void ajouter(Employe e){liste.add(e);}; 
    // ajoute l'objet e dans la collection liste
    
    
    public static void modifier(Employe e){
        for(int i=0;i<liste.size();i++){
            if(liste.get(i).getId()==e.getId()){
                liste.remove(i);
                liste.add(i, e);
            }
        }
       
    }; 
    // remplace par e, l'objet Employe de la liste qui a même id que e
    
    
    public static Employe trouver(int id){return liste.get(id);};
    // renvoie l'objet Employe de la liste qui a l'id passé en paramètre
    
    
    public static void supprimer(int id){liste.remove(id);}; 
    // retirer de la liste, l'objet Employe qui a l'id passé en paramètre
    
    
    public static void supprimer(Employe e){liste.remove(e);};
    // retirer de la liste, l'objet Employe passé en paramètre
    
    
    public static List<Employe> lister(){return liste;}; 
    // renvoyer tous les éléments de la liste
    
    
    public static List<Employe> lister(int debut, int nombre){
        return liste.subList(debut, debut+nombre);
    }; 
    // renvoyer nombre éléments de la liste, commençant à la position debut
}
