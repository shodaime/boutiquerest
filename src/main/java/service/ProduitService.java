package service;

import entities.Produit;
import java.util.List;


public class ProduitService {
    static List<Produit> liste;
    
    public static void ajouter(Produit e){liste.add(e);}; 
    // ajoute l'objet e dans la collection liste
    
    
    public static void modifier(Produit e){
        for(int i=0;i<liste.size();i++){
            if(liste.get(i).getId()==e.getId()){
                liste.remove(i);
                liste.add(i, e);
            }
        }
       
    }; 
    // remplace par e, l'objet Categorie de la liste qui a même id que e
    
    
    public static Produit trouver(int id){return liste.get(id);};
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    
    
    public static void supprimer(int id){liste.remove(id);}; 
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    
    
    public static void supprimer(Produit e){liste.remove(e);};
    // retirer de la liste, l'objet Categorie passé en paramètre
    
    
    public static List<Produit> lister(){return liste;}; 
    // renvoyer tous les éléments de la liste
    
    
    public static List<Produit> lister(int debut, int nombre){
        return liste.subList(debut, debut+nombre);
    }; 
    // renvoyer nombre éléments de la liste, commençant à la position debut
}
