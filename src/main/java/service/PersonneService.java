package service;

import entities.Personne;
import java.util.List;
public class PersonneService {
    static List<Personne> liste;
    
    public static void ajouter(Personne e){liste.add(e);}; 
    // ajoute l'objet e dans la collection liste
    
    
    public static void modifier(Personne e){
        for(int i=0;i<liste.size();i++){
            if(liste.get(i).getId()==e.getId()){
                liste.remove(i);
                liste.add(i, e);
            }
        }
       
    }; 
    // remplace par e, l'objet Personne de la liste qui a même id que e
    
    
    public static Personne trouver(int id){return liste.get(id);};
    // renvoie l'objet Personne de la liste qui a l'id passé en paramètre
    
    
    public static void supprimer(int id){liste.remove(id);}; 
    // retirer de la liste, l'objet Personne qui a l'id passé en paramètre
    
    
    public static void supprimer(Personne e){liste.remove(e);};
    // retirer de la liste, l'objet Personne passé en paramètre
    
    
    public static List<Personne> lister(){return liste;}; 
    // renvoyer tous les éléments de la liste
    
    
    public static List<Personne> lister(int debut, int nombre){
        return liste.subList(debut, debut+nombre);
    }; 
    // renvoyer nombre éléments de la liste, commençant à la position debut
}
