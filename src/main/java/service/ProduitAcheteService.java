package service;

import entities.ProduitAchete;
import java.util.List;


public class ProduitAcheteService {
    static List<ProduitAchete> liste;
    
    public static void ajouter(ProduitAchete e){liste.add(e);}; 
    // ajoute l'objet e dans la collection liste
    
    
    public static void supprimer(ProduitAchete e){liste.remove(e);};
    // retirer de la liste, l'objet Categorie passé en paramètre
    
    
    public static List<ProduitAchete> lister(){return liste;}; 
    // renvoyer tous les éléments de la liste
    
    
    public static List<ProduitAchete> lister(int debut, int nombre){
        return liste.subList(debut, debut+nombre);
    }; 
    // renvoyer nombre éléments de la liste, commençant à la position debut
}
